﻿using System;

namespace Payment_System
{
    class Program
    {
        static void Main(string[] args)
        {
            // Creating a new debit card
            Debit d1 = new Debit();

            // Depositing money into account 
            d1.depositMoney(100);

            // Checking the balance 
            d1.getBalance();

            // Buying a big gift to your girlfriend. Getting ERROR-message.
            d1.payDebit(1480);

            // Buying a little gift to your girlfriend instead.
            d1.payDebit(99);
            // Making sure the balance is reduced
            d1.getBalance();

            //***********************************************************//
            // Creating a new coin
            Coin p1 = new Coin();
            // Open wallet - inherited from the cash-class
            p1.openWallet();
            // Payment with 25 kroner for something that costs 19. Change is returned.
            p1.payCash(25, 19);
            // Sweet sound of coin
            p1.Sound();

            //***********************************************************//
            // Creating a new note
            Note p2 = new Note();
            // Pay with a 500-bill for something that costs 100 kroner. Change is returned.
            p2.payCash(500, 100);
            // Sweet sound of notes
            p2.Sound();

        }
    }
}
