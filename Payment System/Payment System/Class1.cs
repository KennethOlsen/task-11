﻿using System;
//using System.Collections.Generic;
//using System.Text;

namespace Payment_System
{
    public class Payments
    {
        // Creating fields with getters and setters for balance
        public int cost;
        public int balance { get; set; }


        // Making sure that balance are reduced with payment.
        public void pay(int cost)
        {
            balance -= cost;
            Console.WriteLine("You just paid " + cost + " kroner.");
        }
    }

    // Abstract class of card - inherited from payments. 
    public abstract class Card : Payments
    {
        // Checking balance.
        public int getBalance()
        {
            Console.WriteLine("You have " + balance + " in your account.");
            return balance;
        }
        
        // Depositing money into account. Balance is increased with int deposit. 
        public int depositMoney(int deposit)
        {
            balance =+ deposit;
            Console.WriteLine("You have deposited " + deposit + " in your account. Have fun!");
            return balance; 
        }

        // Withdraw a amount of money from account. Returning balance. 
        public int withDrawMoney(int withdraw)
        {
            balance = balance - withdraw;
            Console.WriteLine("You have withdrawn " + cost);
            Console.WriteLine("There are " + balance + " left");
            return balance;

        }
    }

    // Debit-card class inherited from Card-class
    public class Debit : Card
    {

        public int payDebit(int cost)
        {
            if (balance < cost)
            {
                Console.WriteLine("You don't have enough money on your account for this purchase!");
                return balance;
            }
            else if ( balance >= cost)
            {
                balance -= cost;
                return balance; 
            }
            return balance; 
        }
    }

    public class Credit : Card
    {
        // Getting and setting your own credit-limit. In this case - unlimited.
        private int creditLimit { get; set; }

    }

    public abstract class Cash
    { 
        // Cash requires opening a wallet.
        public void openWallet()
        {
            Console.WriteLine("You opened your wallet");
        }

        // Paying cash giving you change back.
        public int payCash(int amount, int cost)
        {
            int change = amount - cost;
            Console.WriteLine("You got " + change + " kroner in change.");
            return change;
        }

    }

    // Note-class inherited from Cash. 
    public class Note : Cash
        {
            // Making note-sound.
            public void Sound()
            {
                Console.WriteLine("The notes makes a crunchy sound in your hand.");
            }
        }

    // Coin-class inherited from Cash.
    public class Coin : Cash
        {
            // Making coin-sound.
            public void Sound()
            {
            Console.WriteLine("Ca-ching");
            }
        }
      
    
}
